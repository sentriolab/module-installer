<?php

namespace Sentrio\Composer;

use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;

class Installer extends LibraryInstaller
{
    /**
     * The Sentrio packages
     *
     * @var array
     */
    public $packages = [
        "sentrio-module",
        "sentrio-theme"
    ];
    
    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        $type = $package->getType();
        $names = $package->getNames();
        
        if (is_array($names)) {
            $names = $names[0];
        }
        
        if ("sentrio-module" == $type) {
            list($vendor, $package) = explode("/", $names);
            
            return "modules/".$vendor."/".$package;
        }
        
        if ("sentrio-theme" == $type) {
            list($vendor, $package) = explode("/", $names);
            
            return "themes/".$vendor."/".$package;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return in_array($packageType, $this->packages);
    }
}